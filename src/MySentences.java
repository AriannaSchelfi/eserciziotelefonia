public class MySentences {

    public static final String callMade = "Chiamata effettuata";
    public static final String creditNotSufficient = "Credito non disponibile, effettuare ricarica";
    public static final String numberExists = "Il numero esiste già: ";
    public static final String numberCreated = "Numero creato: ";
    public static final String numberAccepted = "Numero accettato: ";
}
