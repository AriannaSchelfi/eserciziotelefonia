import java.time.LocalDate;

public class SimHolder {
    private final String name;
    private final String surname;
    private final LocalDate birthdate;

    public SimHolder(final String name, final String surname){
        this.name = name;
        this.surname = surname;
        this.birthdate = null;
    }

    public SimHolder(final String name, final String surname, final LocalDate birthdate){
        this.name = name;
        this.surname = surname;
        this.birthdate = birthdate;
    }

    public String getName() {
        return name;
    }

    public String getSurname(){
        return surname;
    }

    public LocalDate getBirthdate() {
        return birthdate;
    }
}
