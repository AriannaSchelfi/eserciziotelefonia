import java.lang.Math;
import java.util.ArrayList;
import java.util.List;

public class PhoneService {
    final String prefix = "+39 ";
    final int MAX_NUM_GENERATOR = 9;
    final int DIGITS_NUMBER = 10;

    private final List<Sim> createdSim = new ArrayList<>();

    void createSim(SimHolder simHolder, double costPerMinute){
        String cellNumber = generateCellNumber();
        Sim sim = new Sim(cellNumber, simHolder, costPerMinute);
        createdSim.add(sim);
    }

    void deleteSim(Sim simToDelete){
        createdSim.removeIf(sim -> sim.getCellNumber().equals(simToDelete.getCellNumber()));
    }

    public List<Sim> getCreatedSim() {
        return createdSim;
    }

    public Sim getSimBy(String cellNumber){
        return createdSim.stream().filter(sim ->sim.getCellNumber().equals(cellNumber)).findFirst().orElse(null);
    }

    public Sim getSimBy(SimHolder simHolder){
        return createdSim.stream().filter(sim ->sim.getSimHolder().equals(simHolder)).findFirst().orElse(null);
    }

    private String generateCellNumber() {
        StringBuilder cellNumber = new StringBuilder(prefix);

        buildCellNumber(cellNumber);

        while(existsCellNumber(cellNumber)){
            System.out.println(MySentences.numberExists+cellNumber);
            cellNumber = new StringBuilder(prefix);
            buildCellNumber(cellNumber);
            System.out.println(MySentences.numberCreated+cellNumber);
        }

        System.out.println(MySentences.numberAccepted+cellNumber);
        return cellNumber.toString();
    }

    private void buildCellNumber(StringBuilder cellNumber) {
        for (int i = 0; i< DIGITS_NUMBER; i++){
            cellNumber.append((int)(Math.random() * MAX_NUM_GENERATOR +1));
        }
    }

    private boolean existsCellNumber(StringBuilder cellNumber) {
        Sim sim = new Sim(cellNumber.toString());
        return createdSim.contains(sim);
    }
}
