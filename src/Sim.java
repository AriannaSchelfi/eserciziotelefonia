import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

public class Sim{
    private String cellNumber;
    private SimHolder simHolder;
    private double costPerMinute = 1;

    private BigDecimal availableCredit = new BigDecimal(0);
    private final List<PhoneCall> phoneCallList = new ArrayList<>();

    protected Sim(String cellNumber) {
        this.cellNumber = cellNumber;
        this.simHolder =null;
    }

    public Sim(String cellNumber, SimHolder simHolder,double costPerMinute) {
        this.cellNumber = cellNumber;
        this.simHolder = simHolder;
        this.costPerMinute = costPerMinute;
    }

    public void makePhoneCall(String callNumber,int duration){
        BigDecimal cost = new BigDecimal(costPerMinute*duration);
        PhoneCall phoneCall = new PhoneCall(callNumber,duration,cost);
        if(availableCredit.compareTo(phoneCall.getCost()) > 0) {
            phoneCallList.add(phoneCall);
            availableCredit=availableCredit.subtract(phoneCall.getCost());
            System.out.println(MySentences.callMade);
        }
        else{
            System.out.println(MySentences.creditNotSufficient);
        }
    }

    public List<PhoneCall> getPhoneCallList() {
        return phoneCallList;
    }

    public BigDecimal getBilling() {
        //soluzione precedente se prezzo in double:
        //return phoneCallList.stream().mapToDouble(PhoneCall::getCost).sum();

        return phoneCallList.stream().map(PhoneCall::getCost).reduce(BigDecimal.ZERO,BigDecimal::add);
    }

    public BigDecimal getBilling(String phoneNumber){
        //soluzione precedente se prezzo in double
        /* return phoneCallList.stream()
                .filter(phoneCall -> phoneCall.getCalledNumber().equals(phoneNumber))
                .mapToDouble(PhoneCall::getCost).sum();
         */

        return phoneCallList.stream()
                .filter(phoneCall -> phoneCall.getCalledNumber().equals(phoneNumber))
                .map(PhoneCall::getCost)
                .reduce(BigDecimal.ZERO,BigDecimal::add);
    }

    public void rechargeCredit(double money) {
        this.availableCredit =this.availableCredit.add(BigDecimal.valueOf(money));
    }

    public BigDecimal getAvailableCredit() {
        return availableCredit;
    }

    public String getCellNumber() { return cellNumber; }

    public SimHolder getSimHolder() { return simHolder; }

    public double getCostPerMinute() { return costPerMinute; }

    public void setCellNumber(String cellNumber) {
        this.cellNumber = cellNumber;
    }

    public void setSimHolder(SimHolder simHolder) {
        this.simHolder = simHolder;
    }

    public void setCostPerMinute(double costPerMinute) {
        this.costPerMinute = costPerMinute;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Sim sim = (Sim) o;
        return cellNumber.equals(sim.cellNumber);
    }

}
