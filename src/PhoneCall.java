import java.math.BigDecimal;

public class PhoneCall {
    private final String calledNumber;
    private final int minuteDuration;
    private final BigDecimal cost;

    public PhoneCall(final String calledNumber, final int minuteDuration, final BigDecimal cost) {
        this.calledNumber = calledNumber;
        this.minuteDuration = minuteDuration;
        this.cost = cost;
    }

    public String getCalledNumber(){
        return calledNumber;
    }

    public int getMinuteDuration() {
        return minuteDuration;
    }

    public BigDecimal getCost() {return cost;}
}
