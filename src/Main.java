import java.time.LocalDate;
import java.util.List;

public class Main {
    public static void main(String[] args) {
        PhoneService phoneService = new PhoneService();

        SimHolder simHolder = new SimHolder("Mario", "Rossi", LocalDate.of(1997,7,28));
        phoneService.createSim(simHolder,1);

        SimHolder simHolder2 = new SimHolder("Paolo", "Rossi", LocalDate.of(1996,7,28));
        phoneService.createSim(simHolder2,2);

        SimHolder simHolder3 = new SimHolder("Mario", "Bianchi", LocalDate.of(1995,7,28));
        phoneService.createSim(simHolder3,3);

        List<Sim> simList =phoneService.getCreatedSim();
        simList.forEach(s->System.out.println(s.getCellNumber()+" "+s.getSimHolder().getName()+" "+s.getSimHolder().getSurname()));

        //delete sim by sim holder
        System.out.println();
        Sim sim = phoneService.getSimBy(simHolder);
        phoneService.deleteSim(sim);
        simList.forEach(s->System.out.println(s.getCellNumber()+" "+s.getSimHolder().getName()+" "+s.getSimHolder().getSurname()));

        //chiamate sim, credito disponibile e ricarica
        System.out.println();
        Sim sim2 = phoneService.getSimBy(simList.get(0).getCellNumber());//get by phone number
        System.out.println("credito sim "+sim2.getAvailableCredit());
        sim2.makePhoneCall("+39 0000000000", 5);
        sim2.rechargeCredit(20);
        System.out.println("credito sim "+sim2.getAvailableCredit());
        sim2.makePhoneCall("+39 0000000000",5);
        System.out.println("credito sim "+sim2.getAvailableCredit());
        sim2.makePhoneCall("+39 0000000001",2);
        System.out.println("credito sim "+sim2.getAvailableCredit());

        //phonecallList and billing
        List<PhoneCall> phoneList = sim2.getPhoneCallList();
        phoneList.forEach(call -> System.out.println("duration "+call.getMinuteDuration()+" cost "+call.getCost()+" to " +call.getCalledNumber()));
        System.out.println("Total billing "+sim2.getBilling());
        System.out.println("Billing for +39 0000000000 "+sim2.getBilling("+39 0000000000"));
    }
}
